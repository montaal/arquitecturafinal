// Import WebSocketModule
const WebSocket = require('./chatsocket-websocket');

function ChatSocket (server) {

    // latest 100 messages
    let history = [ ];

    // list of currently connected clients (users)
    let clients = [ ];

    // Array with some colors
    const colors = [ 'red', 'green', 'blue', 'magenta', 'purple', 'plum', 'orange' ];

    // ... in random order
    colors.sort(function(a,b) { return Math.random() > 0.5; } );

    WebSocket(server, clients, history, colors);
}

module.exports = ChatSocket;