// Port where we'll run the websocket server
const webSocketsServerPort = 1337;

const ChatSocket = require('../chat.js/index');

const http = require('http');

const server = http.createServer(function(request, response) {
    // Not important for us. We're writing WebSocket server, not HTTP server
});
server.listen(webSocketsServerPort, function() {
    console.log((new Date()) + " Server is listening on port " + webSocketsServerPort);
});

ChatSocket(server);
